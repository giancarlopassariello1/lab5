// Giancarlo Passariello

package com.example;

import static org.junit.Assert.*;

import org.junit.Test;

public class CylinderTest {
    
    
    @Test
    public void testGetVolume() {
        Cylinder c = new Cylinder(4, 3);
        assertEquals("Testing getVolume() method", 48*Math.PI, c.getVolume(), 0.001);
    }

    @Test 
    public void testGetSurfaceArea() {
        Cylinder c = new Cylinder(4, 3);
        assertEquals("Testing getSurfaceArea() method", 56*Math.PI, c.getSurfaceArea(), 0.001);   
    }

    @Test
    public void testGetRadius() {
        Cylinder c = new Cylinder(4, 3);
        assertEquals("Testing getRadius method", 4, c.getRadius(), 0.001);
    }

    @Test
    public void testGetHeight() {
        Cylinder c = new Cylinder(4, 3);
        assertEquals("Testing getHeight method", 3, c.getHeight(), 0.001);
    }

    @Test
    public void testGetHeightIfNegative() {
        Cylinder c = new Cylinder(4, -3);
        assertEquals("Testing getHeight method", -3, c.getHeight(), 0.001);
    }
}

//Tony Nguyen
package com.example;
import static org.junit.Assert.*;
import org.junit.Test;

public class SphereTest {
    
    @Test
    public void testGetVolume(){
        Sphere s = new Sphere(3);
        assertEquals("Testing getVolume method", s.getVolume(), 4/3 * Math.PI * 3 * 3 * 3, 0.001);
    }

    @Test
    public void testGetSurfaceArea(){
        Sphere s = new Sphere(3);
        assertEquals("Testing getSurfaceArea method", s.getSurfaceArea(), 4 * Math.PI * 3 * 3, 0.001);
    }

    @Test
    public void testGetRadius(){
        Sphere s = new Sphere(3);
        assertEquals("Testing getRadius method", s.getRadius(), 3, 0.01);
    }
}

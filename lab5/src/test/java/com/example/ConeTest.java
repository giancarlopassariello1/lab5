//Brian Kirkov

package com.example;

import static org.junit.Assert.*;

import org.junit.Test;

public class ConeTest {
    
    /**
     * Testing the get methods for cone
     */
    @Test
    public void testGetMethods() {
        Cone c = new Cone(2, 5);
        assertEquals("Making sure the correct value for radius is returned", c.getRadius(), 2.0, 0.0000000001);
        assertEquals("Making sure the correct value for height is returned", c.getHeight(), 5.0, 0.0000000001);
    }

    /**
     * Testing the volume method for cone
     */
    @Test
    public void testVolume() {
        Cone c = new Cone(2, 5);
        assertEquals("Making sure the volume is computed correctly", c.getVolume(), 20.9439510239, 0.0000000001);
    }

    /**
     * Testing the surface area method for cone
     */
    @Test
    public void testSurfaceArea() {
        Cone c = new Cone(2, 5);
        assertEquals("Making sure the surface area is computed correctly", c.getSurfaceArea(), 46.4023590073, 0.0000000001);
    }
}

//Tony Nguyen 2131853
package com.example;

public class Cone implements Shape3d {

    private double radius;
    private double height;

    /**
     * Passes values of parameters to cone when initialized
     */
    public Cone(double radius, double height){
        this.radius = radius;
        this.height = height;
    }

    /**
     * Computes The surface area of the
     * @return the volume of cone object
     */
    public double getVolume() {
        return Math.PI * this.radius * this.radius * (this.height/3);
    }
    
    /**
     * Computes The surface area of the
     * @return surface area of cone object
     */
    public double getSurfaceArea() {
        return Math.PI * this.radius * (this.radius + Math.sqrt(this.height * this.height + this.radius * this.radius));
    }

    /**
     * Computes The surface area of the
     * @return The surface area of the
     */
    public double getRadius(){
        return this.radius;
    }

    /**
     * Computes The surface area of the
     * @return the height of cone object
     */

    public double getHeight(){
        return this.height;
    }
}

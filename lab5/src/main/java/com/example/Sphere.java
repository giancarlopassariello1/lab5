// Giancarlo Passariello
package com.example;

public class Sphere implements Shape3d {

    private double radius;

    /**
     * This is the constructor, it sets the value of the radius
     * @param radius  The value of the radius of the sphere
     */
    public Sphere(double radius) {
        this.radius = radius;
    }

    /**
     * Returns volume
     * @return The volume of the sphere
     */
    @Override
    public double getVolume() {
        return (4/3) * Math.PI * (this.radius * this.radius * this.radius);
    }

    /**
     * Returns surface area
     * @return The surface area of the sphere
     */
    @Override
    public double getSurfaceArea() {
        return 4 * Math.PI * (this.radius * this.radius);
    }

    /**
     * Returns the radius
     * @return The radius of the sphere
     */
    public double getRadius() {
        return this.radius;
    }


}
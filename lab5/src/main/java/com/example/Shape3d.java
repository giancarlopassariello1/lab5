package com.example;

public interface Shape3d {
    
    double getVolume();

    double getSurfaceArea();
}

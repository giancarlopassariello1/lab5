//Brian Kirkov

package com.example;

public class Cylinder implements Shape3d {
    
    private double radius;
    private double height;

    /**
     * Passes values of parameters to cylinder when initialized
     * @param r is passed to this.radius
     * @param h is passed to this.height
     */
    public Cylinder(double r, double h) {
        this.radius = r;
        this.height = h;
    }

    /**
     * Computes the volume of a cylinder
     * @return The volume of the cylinder
     */
    @Override
    public double getVolume() {
        double volume = Math.PI * this.radius*this.radius * this.height;
        return volume;
    }
 
    /**
     * Computes the surface area of a cylinder
     * @return The surface area of the cylinder
     */
    @Override
    public double getSurfaceArea() {
        double surfaceArea = (2*Math.PI * this.radius*this.radius) + (2*Math.PI*this.radius*this.height);
        return surfaceArea;
    }

    /**
     * @return The value of this.radius
     */
    public double getRadius() {
        return this.radius;
    }

    /**
     * @return The value of this.height
     */
    public double getHeight() {
        return this.height;
    }
}